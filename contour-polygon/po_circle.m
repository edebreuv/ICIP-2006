%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function circle = po_circle(center, radius, nedges, o_resolution)

%PO_CIRCLE Instantiation of a closed polygon sampling a circle.
%
%   P = po_circle(C, R, N, O_S) computes a closed polygon sampling the circle
%   of center C and radius R with either (a) N edges of equal length (or
%   equivalently N regularly spaced vertices) or (b) a vertex every O_S pixels.
%
%   P: 2x(N+1)-double array
%   C: 2-element-numerical array
%   R: number
%   N: integer
%   O_S: number
%
%   If N is greater than or equal to 3, O_S is ignored and can be omitted.
%   Otherwise, O_S is used to compute the actual number N of edges.
%   (Note: the threshold is 3 since it is (obviously) the minimum number of
%   edges of a non-degenerate closed polygon.)
%
%   Examples
%      P = po_circle([10 10], 6, 20);   % no resolution is passed
%      P = po_circle([10 10], 6, 20, 3);% the passed resolution is ignored
%      P = po_circle([10 10], 6, 0, 3); % the passed resolution is used to
%                                       % compute the number of edges
%
%See also acontour (section Polygon)
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: April 22, 2010

if ac_debug
   assert(isnumeric(center) && (numel(center) == 2), '\n/!\\ AC Toolbox:%s: 1st parameter "center" must be a 2-element-numerical array.', mfilename)
   assert(isnumeric(radius) && (numel(radius) == 1), '\n/!\\ AC Toolbox:%s: 2nd parameter "radius" must be a number.', mfilename)
   assert(isnumeric(nedges) && (numel(nedges) == 1) && (nedges == floor(nedges)), '\n/!\\ AC Toolbox:%s: 3rd parameter "nedges" must be an integer.', mfilename)
   if nedges < 3
      assert(logical(exist('o_resolution', 'var')), '\n/!\\ AC Toolbox:%s: resolution is required when the requested number of edges (%d) is less than 3.', mfilename, nedges)
      assert(isnumeric(o_resolution) && (numel(o_resolution) == 1), '\n/!\\ AC Toolbox:%s: 4th parameter "o_resolution" must be a number.', mfilename)
   end
end

if nedges < 3
   nedges = round(2 * pi * radius / o_resolution);
   if nedges < 3
      nedges = 3;
   end
end

sampling_angles = 2 * pi / nedges * (nedges:-1:1);

circle = [center(1) + radius * sin(sampling_angles); ...
          center(2) + radius * cos(sampling_angles)];

circle = [circle circle(:,1)];
