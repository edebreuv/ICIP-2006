%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function mask = po_mask(polygon, framesize)

%PO_MASK Conversion of a closed polygon into a binary mask.
%
%   M = po_mask(P, S) computes the binary mask M of the closed polygon P limited
%   to the rectangular region with upper left corner (1,1) and lower right
%   corner S.
%
%   M: logical array of dimension size(S)
%   P: 2x*-double array
%   S: 2-element-integer array
%
%   Since po_mask simply calls the function poly2mask (matlab), the polygon is
%   automatically closed if not already.
%   The clipping to the rectangular area is handled by poly2mask.
%
%   (Note: this function was written only to account for the interpretation of
%   vertex coordinates made by the Active Contour Toolbox.)
%
%   Example
%      P = po_circle([50 70], 40, 0, 6);
%      M = po_mask(P, [100 100]);
%      imagesc(M), axis equal, colormap gray
%
%See also acontour (section Polygon), poly2mask (matlab)
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: April 27, 2010

mask = poly2mask(polygon(2,:), polygon(1,:), framesize(1), framesize(2));
