%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function ellipse = po_ellipse(center, radii, tilt, nedges, o_resolution)

%PO_ELLIPSE Instantiation of a closed polygon sampling an ellipse.
%
%   P = po_ellipse(C, R, T, N, O_S) computes a closed polygon sampling the
%   ellipse of center C, radii R, and tilt angle T (in degrees) with either
%   (a) N edges of equal length (or equivalently N regularly spaced vertices)
%   or (b) a vertex every O_S pixels.
%
%   P: 2x(N+1)-double array
%   C: 2-element-numerical array
%   R: 2-element-numerical array
%   T: number
%   N: integer
%   O_S: number
%
%   If N is greater than or equal to 3, O_S is ignored and can be omitted.
%   Otherwise, O_S is used to compute the actual number N of edges.
%   (Note: the threshold is 3 since it is (obviously) the minimum number of
%   edges of a non-degenerate closed polygon.)
%
%   Examples
%      P = po_ellipse([10 10], [6 10], 15, 20);   % no resolution is passed
%      P = po_ellipse([10 10], [6 10], 15, 20, 3);% the passed resolution is ignored
%      P = po_ellipse([10 10], [6 10], 15, 0, 3); % the passed resolution is used to
%                                                 % compute the number of edges
%
%See also acontour (section Polygon)
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: April 27, 2010

if nedges < 3
   h = (diff(radii) / sum(radii))^2;
   arclength = pi * sum(radii) * (1 + 3 * h / (10 + sqrt(4 - 3 * h)));%Ramanujan, 1914

   nedges = round(arclength / o_resolution);
   if nedges < 3
      nedges = 3;
   end
end

sampling_angles = 2 * pi / nedges * (nedges:-1:1);

ellipse = [radii(1) * sin(sampling_angles); ...
           radii(2) * cos(sampling_angles); ...
           ones(1,nedges)];

tilt = tilt * pi / 180;
cos_of_tilt = cos(tilt);
sin_of_tilt = sin(tilt);

affine = [cos_of_tilt, - sin_of_tilt, center(1); ...
          sin_of_tilt,   cos_of_tilt, center(2)];
ellipse = affine * ellipse;

ellipse = [ellipse ellipse(:,1)];
