%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function orientation_or_polygon = po_orientation(polygon, o_orientation)

%PO_ORIENTATION Computes or sets the orientation of a polygon.
%
%   (O)rientation = po_orientation((P)olygon) computes the orientation of the
%   polygon P as a real number O which is positive if P is oriented
%   counterclockwise and negative otherwise. If the polygon is simple, abs(O)
%   should be very close to 2*pi.
%
%   po(L)ygon = po_orientation((P)olygon, O_(O)rientation) returns the polygon L obtained by setting the
%   orientation of P as follows:
%   * if O_O is positive, L is oriented counterclockwise;
%   * if O_O is equal to zero, the orientations of P and L are opposite;
%   * if O_O is negative, L is oriented clockwise.
%
%See also acontour (section Polygon)
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: May 4, 2010

edges = diff(polygon, 1, 2);
lengths = sqrt(sum(edges.^2));

edges(:,end+1) = edges(:,1);
lengths(end+1) = lengths(1);

cosines = sum(edges(:,1:(end-1)) .* edges(:,2:end)) ./ (lengths(1:(end-1)) .* lengths(2:end));
cosines(cosines >  1) =  1;% necessary because of possible
cosines(cosines < -1) = -1;% round-off errors leading to complex angles
angles = acos(cosines);

determinant = edges(1,1:(end-1)) .* edges(2,2:end) - edges(1,2:end) .* edges(2,1:(end-1));

orientation = sum(sign(determinant) .* angles);

if nargin < 2
   orientation_or_polygon = orientation;
else
   switch sign(o_orientation)
      case 1
         if orientation < 0
            orientation_or_polygon = fliplr(polygon);
         else
            orientation_or_polygon = polygon;
         end

      case 0, orientation_or_polygon = fliplr(polygon);

      case -1
         if orientation > 0
            orientation_or_polygon = fliplr(polygon);
         else
            orientation_or_polygon = polygon;
         end
   end
end
