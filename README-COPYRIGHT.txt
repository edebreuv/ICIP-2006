Copyright CNRS/UNS
Contributor(s): Eric Debreuve (between 2006 and 2010)

eric.debreuve@cnrs.fr

This software has been developed by Eric Debreuve, a CNRS employee and member of Laboratory I3S.
Laboratory I3S is a joint lab between the CNRS and UNS (member of UCA).

CNRS: http://www.cnrs.fr/index.html
I3S:  http://www.i3s.unice.fr/en
UNS:  http://unice.fr/en
UCA:  http://univ-cotedazur.fr/en
