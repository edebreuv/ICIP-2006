%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function [segm_context, algo_context] = pc_segmentation(...
   user_data, initial_segm_context, ...
   pcontour_validity, acontour_computation, mask_computation, ...
   energy_function, velocity_amplitude, update_computation, ...
   o_segmentation_prm, o_pcontour_prm, o_interface)
%
%initial_segm_context: a cell containing several structures/real numbers (1)
%                      a structure/real number                           (2)
%                      a structure with a field "pcontours" <=> (1)
%                      a structure with a field "pcontour"  <=> (2)

   %reading of context
   if isstruct(user_data)
      framesize = user_data.framesize;
   else
      if iscell(user_data)
         framesize = size(user_data{1});
      else
         framesize = size(user_data);
      end
      framesize = framesize(1:2);
   end

   [segm_context, algo_context, pcontours, several_pcontours] = s_context(initial_segm_context);
   clear initial_segm_context
   %next_pcontours = cell(size(pcontours));

   %reading of parameters
   if nargin < 11
      o_interface = [];
   if nargin < 10
      o_pcontour_prm = [];
   if nargin < 9
      o_segmentation_prm = [];
         end
      end
   end
   [iteration_limit, step_limit] = s_parameters(o_segmentation_prm, o_pcontour_prm);
   clear o_segmentation_prm o_pcontour_prm

   tasks = {'pcontour', 'movie', 'interruption'};
   if isempty(o_interface)
      o_interface = cell2struct(repmat({@s_mute_interface}, 1, length(tasks)), tasks, 2);
   else
      absent_tasks = find(isfield(o_interface, tasks) == false);
      for task_idx = absent_tasks
         o_interface.(tasks{task_idx}) = @s_mute_interface;
      end
   end

   %internal parameters
   width_of_energy_window = 15;
   convergence_slope      = 5e-4;

   %SEGMENTATION PROCESS>
   ac_energy('(re)start', 1, width_of_energy_window, convergence_slope)
   pc_evolution('initialization', length(pcontours), step_limit)

   %ac_energy('new resolution')
   acontour = acontour_computation(pcontours{1}, 's', user_data);
   o_interface.pcontour('new resolution', acontour, 0, step_limit, iteration_limit)

   before     = now;%for computation time with intermediate times
   patience   = true;
   something  = true;
   descending = true;
   iteration  = 1;

   while descending && (iteration <= iteration_limit) && something
      patience = o_interface.interruption('patience');
      if patience == 0, break, end
      if patience < 0
         keyboard
      end

      if several_pcontours
         segm_context.pcontours = pcontours;
      else
         segm_context.pcontour  = pcontours{1};
      end
      algo_context.iteration = iteration;

      [energy, global_prm] = energy_function(segm_context, algo_context, ...
         acontour_computation, mask_computation, user_data);
      if ~isempty(global_prm)
         cells_of_fieldnames = fieldnames(global_prm);
         for fieldname_idx = 1:length(cells_of_fieldnames)
            segm_context.(cells_of_fieldnames{fieldname_idx}) = ...
               global_prm.(cells_of_fieldnames{fieldname_idx});
         end
      end

      %if ac_energy('update', energy, pc_evolution('evolution rate'))
      if ac_energy('store', energy)
         descending = false;
      else
         max_step = 0;
         for pcontour_idx = length(pcontours):-1:1%so that samples and amplitude have the correct value for display below
            pcontour = pcontours{pcontour_idx};
            if ~isempty(pcontour)
               if several_pcontours
                  algo_context.pcontour_index = pcontour_idx;
               end

               update = update_computation(segm_context, algo_context, ...
                  acontour_computation, mask_computation, velocity_amplitude, user_data);

               %normalization
               norm_of_update = sqrt(sum(update(:).^2));
               if norm_of_update > 0
                  update = update / norm_of_update;
               end

               optimal_step = n_step(pcontour, update, norm_of_update, pc_evolution('steps', pcontour_idx), energy);
               update = optimal_step * update;
               %pcontour = pcontour + update;
               pcontours{pcontour_idx} = pcontour + update;

               pc_evolution('update', pcontour_idx, update, optimal_step)
               max_step = max(optimal_step, max_step);

               %next_pcontours{pcontour_idx} = pcontour;
            end
         end
         %pcontours = next_pcontours;
         something = any(~cellfun(@isempty, pcontours));

         if ~isequal(o_interface.pcontour, @s_mute_interface)
            samples = acontour_computation(pcontours{1}, 'p', user_data);
            samples(:,end) = [];
            o_interface.pcontour('evolution update', samples, iteration, ones(1,size(samples,2)), ac_energy('energies'))%display
         end
         if ~isequal(o_interface.movie, @s_mute_interface)
            acontour = acontour_computation(pcontours{1}, 's', user_data);
            o_interface.movie('movie update', acontour, iteration)
         end

         descending = (max_step > 0);
         iteration = iteration + 1;
      end
   end

   if patience == 0
      convergence_msg = 'cancelled after ';
   elseif descending
      convergence_msg = 'no conv. after ';
   else
      convergence_msg = '';
   end
   duration = now - before;
   disp([convergence_msg ...
      int2str(iteration - 1) ' update(s) - ' ...
      datestr(duration, 'HH') 'h ' ...
      datestr(duration, 'MM') 'm ' ...
      datestr(duration, 'SS.FFF') 's (cumulative)'])
   %<SEGMENTATION PROCESS

   %writing of context
   if several_pcontours
      segm_context.pcontours = pcontours;
   else
      segm_context.pcontour  = pcontours{1};
   end
   algo_context.iteration = iteration - 1;
   algo_context.status = pc_evolution('steps (str)', 1);


      function optimal_step = n_step(pcontour, update, norm_of_update, previous_steps, energy)
         %framesize, step_limit, pcontour_validity, acontour_computation, mask_computation, ...
         %energy_function, segm_context, algo_context, user_data

         %fast or full computation
         if step_limit >= 0
            if step_limit > 0
               optimal_step = step_limit;
            else
               optimal_step = norm_of_update;
            end
            return
         end
         current_limit = - step_limit;

         %internal parameters
         increment_limit  = 0.1;%in parameter units
         intervals        = 5;
         allowed_increase = 1.7;%to allow a bigger step than the recent ones
         ad_hoc           = 5;

         %list of energy discretization steps
         accounting_since = max(1, length(previous_steps) - floor(min(framesize) / (ad_hoc * mean(previous_steps))));
         current_limit = min(current_limit, allowed_increase * mean(previous_steps(accounting_since:end)));
         increment = max((current_limit - increment_limit) / intervals, increment_limit);
         list_of_steps = increment_limit:increment:current_limit;

         %energy discretization
         current_pcontour = pcontour;
         energies = [energy zeros(size(list_of_steps))];
         energy_idx = 1;
         for step = list_of_steps
            pcontour = current_pcontour + step * update;

            if pcontour_validity(pcontour)
               if algo_context.pcontour_index == 0
                  segm_context.pcontour = pcontour;
               else
                  segm_context.pcontours{algo_context.pcontour_index} = pcontour;
               end
               energy_idx = energy_idx + 1;
               energies(energy_idx) = energy_function(segm_context, algo_context, acontour_computation, mask_computation, user_data);
            else
               break
            end
         end

         %polynomial fit and minimization
         if energy_idx == 1
            optimal_step = 0;
         else
            energies = energies(1:energy_idx);
            list_of_steps = [0 list_of_steps(1:(energy_idx-1))];
            energies = (energies - mean(energies)) / std(energies);

            if energy_idx == 2
               degree = 1;
            elseif energy_idx < 5
               degree = 2;
            else
               degree = 4;
            end
            fit = polyfit(list_of_steps, energies, degree);
            options = optimset('Display', 'off', 'TolX', 0.1 * increment_limit);
            optimal_step = fminbnd(@(s) polyval(fit,s), 0, list_of_steps(end), options);
         end
      end
end



function [segm_context, algo_context, pcontours, several_pcontours] = ...
   s_context(initial_segm_context)

   if iscell(initial_segm_context)
      segm_context.pcontours = initial_segm_context;
   else
      if any(isfield(initial_segm_context, {'pcontour' 'pcontours'}))
         segm_context = initial_segm_context;
      else
         segm_context.pcontour = initial_segm_context;
      end
   end
   if isfield(segm_context, 'pcontour')
      pcontours = {segm_context.pcontour};
      several_pcontours = false;
   else
      pcontours = segm_context.pcontours;
      several_pcontours = true;
   end

   if ~several_pcontours
      algo_context.pcontour_index = 0;
   end
end



function [iteration_limit, step_limit] = s_parameters(segmentation_prm, pcontour_prm)

   parameter_names = {'it' 'iteration' 'iterations' 'MaxIter'};
   which_name = find(isfield(segmentation_prm, parameter_names));
   if isempty(which_name)
      iteration_limit = 50;
   else
      iteration_limit = segmentation_prm.(parameter_names{which_name(1)});
   end

   parameter_names = {'step' 'step_limit'};
   which_name = find(isfield(pcontour_prm, parameter_names));
   if isempty(which_name)
      step_limit = 1;
   else
      step_limit = pcontour_prm.(parameter_names{which_name(1)});
   end
end



function o_patience = s_mute_interface(task, varargin)
   if strcmp(task, 'patience'), o_patience = true; end
end
