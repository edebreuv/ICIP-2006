%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function varargout = pc_evolution(task, about_pcontours, varargin)
%
%to be done

persistent p_mean_updates p_step_storage p_steps
%(pcontour index, iteration)

switch task
   case 'initialization'%, number of pcontours, fake "previous" optimal step
      p_mean_updates = zeros(about_pcontours, 1);
      p_mean_updates(:,1) = 1;%fake "previous" mean update

      p_step_storage = (varargin{1} <= 0);
      if p_step_storage
         p_steps = zeros(about_pcontours, 1);
         p_steps(:,1) = - varargin{1};%fake "previous" step
      end

   case 'update'%, pcontour index, update, step
      if ~any(p_mean_updates(:,end) < 0)
         p_mean_updates(:,end+1) = -1;
         if p_step_storage
            p_steps(:,end+1) = 0;
         end
      end
      p_mean_updates(about_pcontours,end) = sqrt(sum(varargin{1}(:).^2));
      if p_step_storage
         p_steps(about_pcontours,end) = varargin{2};
      end

   case 'evolution rate'%
      varargout{1} = mean(p_mean_updates(:,end));

   case 'steps'%, pcontour index
      if p_step_storage
         varargout{1} = p_steps(about_pcontours,:);
      else
         varargout{1} = [];
      end

   case 'steps (str)'%, pcontour index
      if p_step_storage
         varargout{1} = num2str(p_steps(about_pcontours,2:end), '%.3g, ');%the fake step is not output
         varargout{1}([end-1 end]) = [];
      else
         varargout{1} = 'constant step';
      end

   case 'plot'%, pcontour index
      if nargin < 2
         about_pcontours = 1;
      end
      figure('Name', ['Evolution of pcontour ' int2str(about_pcontours)])
      subplot(2, 1, 1);
      if p_step_storage
         plot(p_steps(about_pcontours,2:end), 'LineWidth', 1.5)%the fake step is not plotted
      else
         plot(ones(1, size(p_mean_updates,2)-1), 'LineWidth', 1.5)
      end
      set(get(gca, 'XLabel'), 'String', 'Iterations')
      set(get(gca, 'YLabel'), 'String', 'Steps')
      set(gca, 'XLim', [1, size(p_mean_updates,2)-1])
      subplot(2, 1, 2);
      plot(p_mean_updates(about_pcontours,2:end), 'LineWidth', 1.5)%the fake mean update is not plotted
      set(get(gca, 'XLabel'), 'String', 'Iterations')
      set(get(gca, 'YLabel'), 'String', 'Mean updates')
      set(gca, 'XLim', [1, size(p_mean_updates,2)-1])

   case 'clear', clear(mfilename)
   otherwise,    help(mfilename)
end
