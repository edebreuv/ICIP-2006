%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function o_handles = ac_plot(o_axes, acontour, o_style)

%AC_PLOT Plots an active contour in a window of given height.
%
%   O_(H)andles = ac_plot(O_a(X)es, (A)contour, O_(S)tyle) plots the active
%   contour A in the (optionally specified) axes O_X with the (optionally
%   specified) plotting style O_S. The default value for O_X is the output of
%   gca. The default value for O_S is 'eb vgo' (see details below).
%
%   O_H: array of graphics handles (if requested)
%   O_X: graphics handle (if specified)
%   A:   active contour
%   O_S: character string (if specified)
%
%   Note: if the plotting is performed in axes with the Y-axis pointing
%   classically upward (and with the hold property on), then the active contour
%   will be plotted upside down due to the interpretation of coordinates of the
%   Active Contour Toolbox. This function is adapted to plot over an image
%   (which should be the usual case in segmentation), which, by default in
%   matlab, has a downward pointing Y-axis.
%
%   O_S is a string composed of 2-to-4-character sets, optionally followed by a
%   number, separated by white spaces. There can be as many as 5 sets. The first
%   character of a set determines the type of active contour element to plot.
%   * First character
%      e for edges, v for vertices, i for indices, t for tangents, or n for
%      normals
%   * Second character
%      A color among: r, g, b, c, m, y, k, or w (see LineSpec)
%   * Either (a) - Third (and last non numerical) character
%      # For e: - or :
%      # For v: +, o, *, ., x, s, d, ^, v, >, <, p, or h (see LineSpec)
%   * Or (b)     - Third and fourth characters
%      # For e only: -- or -. (see LineSpec)
%   * A number immediately following the last character
%      # For e: linewidth in points (1 point = 1/72 inch)
%      # For v: marker size in points
%      # For i: distance between indices and vertices (default is 0.5)
%
%   O_H is the array of the graphics handles of the plotted active contour
%   elements.
%
%   Examples
%      P = po_ellipse([10 15], [6 10], 15, 20);
%      figure, image(255*ones(30)), colormap gray, axis image, hold on
%      ac_plot(P)
%
%      figure, image(255*ones(30)), colormap gray, axis image, hold on
%      ac_plot(P, 'eb--2 vgo nr tc ik')
%
%      figure, gca; axis equal, hold on
%      ac_plot(P, 'eb--2 vgo nr tc ik')% the actice contour is upside down
%
%See also acontour
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: May 5, 2010

%% parameter processing
if ~exist('o_style', 'var')
   o_style = [];

   if ~ishandle(o_axes)
      if nargin > 1
         o_style = acontour;
      end
      acontour = o_axes;
      o_axes = [];
  %else there must be a second parameter: the active contour
   end
end

if isstruct(acontour)
   curve = acontour;
   [breaks, ~, ~, ~, ~] = unmkpp(curve);% COMPATIBILITY: breaks, u_coefs, u_npieces, u_order, u_dimension
   acontour = ppval(curve, linspace(breaks(1), breaks(end), 100));
else
   if iscell(acontour)
      curve = cellfun(@ac_spline, acontour);
   else
      curve = ac_spline(acontour);
   end
end

%% plot styles
if isempty(o_style)
   edge_style    = 'b';
   vertex_style  = 'go';
   index_color   = '';
   tangent_color = '';
   normal_color  = '';
else
   edge_style    = '';
   vertex_style  = '';
   index_color   = '';
   tangent_color = '';
   normal_color  = '';

   style_starts = [1, strfind(o_style, ' ')+1];% still valid if strfind returns []
   style_ends   = [style_starts(2:end)-2, length(o_style)];% still valid if starts = 1
   for style_idx = 1:length(style_starts)
      switch o_style(style_starts(style_idx))
         case 'e', edge_style    = o_style((style_starts(style_idx)+1):style_ends(style_idx));
         case 'v', vertex_style  = o_style((style_starts(style_idx)+1):style_ends(style_idx));
         case 'i', index_color   = o_style((style_starts(style_idx)+1):style_ends(style_idx));
         case 't', tangent_color = o_style((style_starts(style_idx)+1):style_ends(style_idx));
         case 'n', normal_color  = o_style((style_starts(style_idx)+1):style_ends(style_idx));
      end
   end
end
if isempty(edge_style)
   linewidth = [];
else
   [edge_style, linewidth] = s_style_and_size(edge_style);
end
if isempty(vertex_style)
   marker_size = [];
else
   [vertex_style, marker_size] = s_style_and_size(vertex_style);
   if isempty(marker_size)
      if ~isempty(linewidth)
         marker_size = 2.25 * linewidth;
      end
   elseif isempty(linewidth) && ~isempty(edge_style)
      linewidth = marker_size / 2.25;
   end
end
if ~isempty(index_color)
   [index_color, idx_vtx_distance] = s_style_and_size(index_color);
   if isempty(idx_vtx_distance)
      idx_vtx_distance = 0.5;
   end
end

%% default output (depends on "plot styles")
if nargout > 0
   if ~isempty(edge_style)
      if iscell(acontour)
         edges_handles = zeros(1,length(acontour));
      else
         edges_handles = zeros(1);
      end
   end
   if isempty(vertex_style)
      vertices_handles = [];
   end
   if isempty(tangent_color)
      tangents_handles = [];
   end
   if isempty(normal_color)
      normals_handles = [];
   end
end

%% figure & axes local settings
if isempty(o_axes)
   parent = gcf;
   o_axes = gca;
else
   parent = get(o_axes, 'Parent');
end
hold_was_off = (ishold(o_axes) == 0);
if hold_was_off
   clf(parent)
   o_axes = axes('Parent', parent);
   set(o_axes, 'YDir', 'reverse')
   hold(o_axes, 'on')
end

%% active contour processing & sampling (before any plot)
if iscell(acontour)
   acontour = cellfun(@(sub_ac) flipud(sub_ac), acontour, 'UniformOutput', false);
else
   acontour = flipud(acontour);
end
if ~isempty(vertex_style) || ~isempty(index_color) ...
      || ~isempty(tangent_color) || ~isempty(normal_color)
   nproperties = 1;
   if ~isempty(tangent_color)
      nproperties = 2;
   end
   if ~isempty(index_color) || ~isempty(normal_color)
      nproperties = nproperties + 1;
   end

   switch nproperties
      case 2
         if isempty(tangent_color)
            normals = ac_sampling(curve, 'n');
            if iscell(normals)
               normals = [normals{:}];
            end
            normals = flipud(normals);
         else
            tangents = ac_sampling(curve, 't');
            if iscell(tangents)
               tangents = [tangents{:}];
            end
            tangents = flipud(tangents);
         end

      case 3
         [tangents, normals] = ac_sampling(curve, 'tn');
         if iscell(tangents)
            tangents = [tangents{:}];
            normals  = [normals{:}];
         end
         tangents = flipud(tangents);
         normals  = flipud(normals);
   end

   if iscell(acontour)
      if ~isempty(index_color)
         indices = cellfun(@(element) 1:(size(element,2)-1), acontour, 'UniformOutput', false);
         indices = [indices{:}];
      end
   elseif ~isempty(index_color)
      indices = 1:(size(acontour,2)-1);
   end
end

%% edge plot
if ~isempty(edge_style)
   if length(edge_style) > 1
      properties = {'Parent', o_axes, 'Color', edge_style(1), 'LineStyle', edge_style(2:end)};
   else
      properties = {'Parent', o_axes, 'Color', edge_style};
   end
   if ~isempty(linewidth)
      properties = [properties {'LineWidth', linewidth}];
   end

   if ~iscell(acontour)
      acontour = {acontour};
   end
   for idx = 1:length(acontour)
      if nargout > 0
         edges_handles(idx) = line(acontour{idx}(1,:), acontour{idx}(2,:), properties{:});
      else
         line(acontour{idx}(1,:), acontour{idx}(2,:), properties{:})
      end
   end
   if length(acontour) > 1
      acontour = cellfun(@(element) element(:,1:(end-1)), acontour, 'UniformOutput', false);
      acontour = [acontour{:}];
   else
      acontour = acontour{1};
      acontour(:,end) = [];
   end
end

%% vertex plot
if ~isempty(vertex_style)
   if length(vertex_style) > 1
      properties = {'Parent', o_axes, 'LineStyle', 'none', 'MarkerEdgeColor', vertex_style(1), 'Marker', vertex_style(2)};
   else
      properties = {'Parent', o_axes, 'LineStyle', 'none', 'MarkerEdgeColor', vertex_style};
   end
   if lower(vertex_style(1)) == vertex_style(1)
      properties = [properties {'MarkerFaceColor', vertex_style(1)}];
   end
   if ~isempty(marker_size)
      properties = [properties {'MarkerSize', marker_size}];
   end

   if nargout > 0
      vertices_handles = line(acontour(1,:), acontour(2,:), properties{:});
   else
      line(acontour(1,:), acontour(2,:), properties{:})
   end
end

%% index plot
if ~isempty(index_color)
   normals = idx_vtx_distance * normals * mean(sqrt(sum(diff(acontour, 1, 2).^2)));
   origins = acontour - normals;
   indices = num2str(indices,'%i|');
   indices(end) = [];

   if nargout > 0
      indices_handles = text(origins(1,:), origins(2,:), num2str(indices), ...
         'Parent', o_axes, 'HorizontalAlignment', 'center', 'Color', index_color);
   else
      text(origins(1,:), origins(2,:), num2str(indices), ...
         'Parent', o_axes, 'HorizontalAlignment', 'center', 'Color', index_color)
   end
end

%% vertex tangent plot
if ~isempty(tangent_color)
   origins = acontour - 0.5 * tangents;

   if nargout > 0
      tangents_handles = quiver(o_axes, origins(1,:), origins(2,:), ...
         tangents(1,:), tangents(2,:), 0, tangent_color);
   else
      quiver(o_axes, origins(1,:), origins(2,:), ...
         tangents(1,:), tangents(2,:), 0, tangent_color)
   end
end

%% vertex normal plot
if ~isempty(normal_color)
   if isempty(index_color)
      normals = 0.5 * normals * mean(sqrt(sum(diff(acontour, 1, 2).^2)));
   end
   if nargout > 0
      normals_handles = quiver(o_axes, acontour(1,:), acontour(2,:), ...
         normals(1,:), normals(2,:), 0, normal_color);
   else
      quiver(o_axes, acontour(1,:), acontour(2,:), ...
         normals(1,:), normals(2,:), 0, normal_color)
   end
end

%% figure & axes original settings (after all the plots)
if hold_was_off
   hold(o_axes, 'off')
end

%% output (after all the plots)
if nargout > 0
   if isempty(edge_style)
      edges_group = [];
   elseif length(edges_handles) > 1
      edges_group = hggroup('Parent', o_axes);
      set(edges_handles, 'HitTest', 'off', 'Parent', edges_group)
   else
      edges_group = edges_handles;
   end
   if isempty(index_color)
      indices_group = [];
   elseif length(indices_handles) > 1
      indices_group = hggroup('Parent', o_axes);
      set(indices_handles, 'HitTest', 'off', 'Parent', indices_group)
   else
      indices_group = indices_handles;
   end
   o_handles = [edges_group; vertices_handles; indices_group; tangents_handles; normals_handles];
end



%%
function [style, and_size] = s_style_and_size(style_and_size)

start_of_size = length(style_and_size);
and_size = str2double(style_and_size(end));
while ~isnan(and_size)
   if (and_size < 0) || (style_and_size(start_of_size) == '+')
      break
   end
   start_of_size = start_of_size - 1;
   and_size = str2double(style_and_size(start_of_size:end));
end
if start_of_size == length(style_and_size)
   style = style_and_size;
   and_size = [];
else
   style = style_and_size(1:start_of_size);
   and_size = str2double(style_and_size((start_of_size+1):end));
end
