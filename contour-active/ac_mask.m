%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function mask = ac_mask(acontour, framesize)

%AC_MASK Conversion of an active contour into a binary mask.
%
%   M = ac_mask(A, S) computes the binary mask M of the active contour A limited
%   to the rectangular region with upper left corner (1,1) and lower right
%   corner S.
%
%   M: single-byte-integer array of dimension size(S)
%   A: active contour
%   S: 2-element-integer array
%
%   The clipping to the rectangular area is handled by poly2mask (see po_mask).
%
%   The validity of the active contour is not checked. In particular, if it is
%   composed of intersecting connected components, the ouput will not be a
%   binary mask since the mask is computed as the sum of the connected
%   components.
%
%   Example
%      A = ac_bubbles([100 100], 0, 2);
%      M = ac_mask(A, [100 100]);
%      imagesc(M), axis equal, colormap gray
%
%See also acontour, po_mask, poly2mask (matlab)
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: April 27, 2010

mask = zeros(framesize, 'int8');

if iscell(acontour)
   for idx = 1:length(acontour)
      mask = mask + sign(po_orientation(acontour{idx})) * int8(po_mask(acontour{idx}, framesize));
   end
else
   mask = sign(po_orientation(acontour)) * int8(po_mask(acontour, framesize));
end
