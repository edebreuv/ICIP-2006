%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function varargout = ac_sampling(acontour, o_properties)
%ac_sampling: sampling of an active contour
%   s = ac_sampling(a, o_p) samples an active contour, a, returning the
%   requested properties, o_p. o_p is a string of up to 6 characters among B, b,
%   s, t, n, and c, without repetition, corresponding to the breaks (B or b),
%   the samples, the tangents, the normals, and the curvatures of the active
%   contour, respectively. 'B' corresponds to the complete list of breaks with
%   a(breaks(end)) being equal to a(breaks(1)) while 'b' corresponds to the
%   complete list of breaks but the last one. The other properties are computed
%   at the breaks of this latter list. The tangents are not normalized. The
%   normals are normalized and point inward if the active contour is oriented
%   counterclockwise.
%   The properties are returned in the same order as o_p spells. By default, o_p
%   is taken equal to 'bsn'.
%   If the active contour is composed of several single active contours, each
%   property is returned in a cell array, each cell corresponding to a single
%   active contour.
%
%See also acontour.
%
%Active Contour Toolbox by Eric Debreuve
%Last update: July 21, 2006

if nargin < 2
   o_properties = 'bsn';
end

breaks = cell(1,length(acontour));
for subac_idx = 1:length(acontour)
   breaks{subac_idx} = ppbrk(acontour(subac_idx), 'breaks');
end

where = find(o_properties == 'B');
if ~isempty(where)
   varargout{where} = breaks;
end

for subac_idx = 1:length(acontour)
   breaks{subac_idx}(end) = [];
end

where = find(o_properties == 'b');
if ~isempty(where)
   varargout{where} = breaks;
end

where = find(o_properties == 's');
if ~isempty(where)
   samples = cell(1,length(acontour));
   for subac_idx = 1:length(acontour)
      samples{subac_idx} = ppval(acontour(subac_idx), breaks{subac_idx});
   end
   varargout{where} = samples;
end

if ~isempty(intersect(o_properties, 'tnc'))
   tangents = cell(1,length(acontour));
   for subac_idx = 1:length(acontour)
      tangents{subac_idx} = ppval(fnder(acontour(subac_idx)), breaks{subac_idx});
   end
   where = find(o_properties == 't');
   if ~isempty(where)
      varargout{where} = tangents;
   end

   if ~isempty(intersect(o_properties, 'nc'))
      tangent_norms = cell(1,length(acontour));
      for subac_idx = 1:length(acontour)
         tangent_norms{subac_idx} = sqrt(sum(tangents{subac_idx}.^2));
      end

      where = find(o_properties == 'n');
      if ~isempty(where)
         normals = cell(1,length(acontour));
         for subac_idx = 1:length(acontour)
            normals{subac_idx} = flipud(tangents{subac_idx}) ./ ...
               [- tangent_norms{subac_idx}; tangent_norms{subac_idx}];
         end
         varargout{where} = normals;
      end

      where = find(o_properties == 'c');
      if ~isempty(where)
         curvatures = cell(1,length(acontour));
         for subac_idx = 1:length(acontour)
            second_derivatives = ppval(fnder(acontour(subac_idx), 2), breaks{subac_idx});
            curvatures{subac_idx} = diff(flipud(tangents{subac_idx}) .* second_derivatives) ...
               ./ (tangent_norms{subac_idx}.^3);
         end
         varargout{where} = curvatures;
      end
   end
end

if length(varargout{1}) == 1
   for a = 1:length(varargout)
      varargout{a} = varargout{a}{1};
   end
end
