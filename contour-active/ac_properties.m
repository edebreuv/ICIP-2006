%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function varargout = ac_properties(acontour, o_details)
%
%different properties of the spline in ppform
%
%o_details {'n'}: a character string composed of
%'t' for tangents, 'n' for normals, and 'c' for curvature. there should be no
%repetitions and the properties are returned in the same order as o_details spells
%tangents  = not normalized
%normals   = inward, not normalized
%curvature = not normalized by the cube of the norm of the tangent

if nargin < 2
   o_details = 'n';
end

tangent = cell(1,length(acontour));
for subac_idx = 1:length(acontour)
   tangent{subac_idx} = fnder(acontour(subac_idx));
end
where = find(o_details == 't');
if ~isempty(where)
   varargout{where} = tangent;
end

where = find(o_details == 'n');
if ~isempty(where)
   normal = cell(1,length(acontour));
   for subac_idx = 1:length(acontour)
      normal{subac_idx} = fncmb(ac_inverse(tangent{subac_idx}), '*', [-1;1]);
   end
   varargout{where} = normal;
end

where = find(o_details == 'c');
if ~isempty(where)
   curvature = cell(1,length(acontour));
   for subac_idx = 1:length(acontour)
      [first_tangent, second_tangent] = ac_coordinates(tangent{subac_idx});
      curvature{subac_idx} = fncmb(...
         fncmb(first_tangent,  '*', fnder(second_tangent)), '-',...
         fncmb(second_tangent, '*', fnder(first_tangent)));
   end
   varargout{where} = curvature;
end

if length(varargout{1}) == 1
   for a = 1:length(varargout)
      varargout{a} = varargout{a}{1};
   end
end
