%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function varargout = ac_energy(task, varargin)

%AC_ENERGY Stores the successive energies of a set of active contours and estimates if convergence has been reached.
%
%   O_C = ac_energy((T)ask, ...) performs a task T related to the energy of a
%   set of active contours according to some arguments. The task T is among
%   '(re)start', 'store', 'energies', 'slopes', 'plot', and 'clear'.
%   Note that the convergence test is heuristic.
%
%   ac_energy('(re)start', (W)idth of window, (C)onvergence slope) must be
%   called once per evolution session before any other task. If the segmentation
%   is done at a single resolution, there is only one session. Otherwise, the
%   convergence at one resolution and switching to the next, finer resolution
%   marks the beginning of a new session.
%   W indicates the number of successive energies used to estimate if
%   convergence has been reached. C indicates the absolute value of the energy
%   "derivative" below which convergence is assumed to have been reached.
%
%   (C)onvergence = ac_energy('store', (E)nergy) stores the new energy E of the
%   set of active contours resulting from a round of evolution of the contours,
%   and returns the convergence status C. C is true if, in light of this new
%   energy, convergence is assumed to have been reached.
%
%   (E)nergies = ac_energy('energies') and (S)lopes = ac_energy('slopes')
%   respectively return the energies (successive inputs E passed to the 'store'
%   task) and the slopes computed from these energies to decide convergence.
%
%   ac_energy('plot') opens a figure and plots the absolute energies.
%
%   ac_energy('clear') clears the persistent variables. Most of the tasks
%   produce an error if called afterward.
%
%See also acontour
%
%Active Contour Toolbox -- Eric Debreuve
%Last update: May 4, 2010

persistent r_width_of_window r_convergence_slope ...
   r_indices_of_iterations r_sum_of_indices r_difference_of_sums ...
   r_energies r_slopes% arrays indexed by (iteration)

switch task
   case '(re)start'%, width of energy window, convergence slope
      r_width_of_window   = varargin{1};
      r_convergence_slope = varargin{2};

      r_indices_of_iterations = 1:r_width_of_window;
      r_sum_of_indices        = ((r_width_of_window + 1) * r_width_of_window) / 2;
      sum_of_squared_indices  = ((2 * r_width_of_window + 1) * r_sum_of_indices) / 3;
      r_difference_of_sums    = sum_of_squared_indices - r_sum_of_indices^2 / r_width_of_window;

      r_energies = [];
      r_slopes   = [];

   case 'store'%, energy
      iteration = length(r_energies) + 1;
      r_energies(iteration) = varargin{1};

      if length(r_energies) >= r_width_of_window
         recent_energies = r_energies((end - r_width_of_window + 1):end);

         r_slopes(iteration) = (sum(r_indices_of_iterations .* recent_energies) - ...
            r_sum_of_indices * sum(recent_energies) / r_width_of_window) / r_difference_of_sums;

         if abs(r_slopes(iteration)) < r_convergence_slope
            varargout{1} = true;
         else
            noscillations = sum(logical(diff(sign(diff(recent_energies)))));
            avg_energy = mean(recent_energies);
            above_idx = find(recent_energies > avg_energy);
            below_idx = find(recent_energies < avg_energy);
            % an oscillating behavior with no global energy decrease is
            % characterized by a high number of oscillations (compared to the
            % length of the observation window) and the energies above and below
            % their average value being evenly "iteration"-distributed within
            % the observation window
            if (noscillations > 0.8 * r_width_of_window) && ...
               (abs(mean(above_idx) - mean(below_idx)) < 0.1 * r_width_of_window)
               varargout{1} = true;
            else
               varargout{1} = false;
            end
         end
      else
         r_slopes(iteration) = NaN;
         varargout{1} = false;
      end

   case 'energies', varargout{1} = r_energies;
   case 'slopes',   varargout{1} = r_slopes;

   case 'plot'
      figure('Name', 'Energy')
      plot(r_energies, 'LineWidth', 1.5)
      set(get(gca, 'XLabel'), 'String', 'Iterations')
      set(get(gca, 'YLabel'), 'String', 'Energy')
      set(gca, 'XLim', [1 length(r_energies)])

   case 'clear', clear(mfilename)
   otherwise,    help(mfilename)
end
