%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function update = icip2006_update(segm_context, algo_context, acontour_computation, mask_computation, velocity_amplitude, user_data)

pcontour = segm_context.pcontour;

acontour = acontour_computation(pcontour, 's', user_data);
[breaks, samples] = ac_sampling(acontour, 'Bs');
samples(:,end+1) = samples(:,1);
normals = ac_properties(acontour, 'n');

gl_prm = fn_gausslegendre([], 0, 1);
abscissa = gl_prm(2:end);

switch numel(pcontour)
   case 3
      field_1 = repmat([1;0], 1, length(abscissa));
      field_2 = repmat([0;1], 1, length(abscissa));

   case 5
      field_1 = repmat([1;0], 1, length(abscissa));
      field_2 = repmat([0;1], 1, length(abscissa));

      %this is from po_ellipse
      radii = pcontour([3 4]);
      tilt  = pcontour(5);
      h = (diff(radii) / sum(radii))^2;
      arclength = pi * sum(radii) * (1 + 3 * h / (10 + sqrt(4 - 3 * h)));
      number_of_edges = round(arclength / user_data.resolution);
      sampling_angles = 2 * pi / number_of_edges * (number_of_edges:-1:1);
      sampling_angles(end+1) = sampling_angles(1);
      tilt = tilt * pi / 180;
      cos_of_tilt = cos(tilt);
      sin_of_tilt = sin(tilt);
      in_place = [cos_of_tilt, - sin_of_tilt; ...
                  sin_of_tilt,   cos_of_tilt];
      out_of_place = transpose(in_place);

   otherwise
      weights = {abscissa.^3/6, 1/6 + 0.5 * abscissa .* (1 + abscissa .* (1 - abscissa))};
      weights{3} = fliplr(weights{2});
      weights{4} = fliplr(weights{1});
end

update = zeros(size(pcontour));

for sample_index = 1:(size(samples,2) - 1)
   hires_breaks = abscissa * (breaks(sample_index+1) ...
      - breaks(sample_index)) + breaks(sample_index);

   hires_normals = fnval(normals, hires_breaks);
   invnorm_of_normals = 1 ./ sqrt(sum(hires_normals.^2));
   hires_normals = hires_normals .* [invnorm_of_normals; invnorm_of_normals];
   hires_samples = fnval(acontour, hires_breaks);
   hires_velocities = velocity_amplitude(hires_samples, segm_context, algo_context, acontour_computation, mask_computation, user_data);

   if (numel(pcontour) == 3) || (numel(pcontour) == 5)
      field = field_1;
      update(1) = update(1) - fn_gausslegendre(@n_integrand, gl_prm);

      field = field_2;
      update(2) = update(2) - fn_gausslegendre(@n_integrand, gl_prm);
   end

   switch numel(pcontour)
      case 3
         field = - hires_normals;
         update(3) = update(3) - fn_gausslegendre(@n_integrand, gl_prm);

      case 5
         hires_samples(1,:) = hires_samples(1,:) - pcontour(1);
         hires_samples(2,:) = hires_samples(2,:) - pcontour(2);
         hires_samples = out_of_place * hires_samples;
         hires_angles = atan2(hires_samples(1,:), hires_samples(2,:));

         sin_of_angle = sin(hires_angles);
         field = [sin_of_angle; zeros(size(abscissa))];
         field = in_place * field;
         update(3) = update(3) - fn_gausslegendre(@n_integrand, gl_prm);

         cos_of_angle = cos(hires_angles);
         field = [zeros(size(abscissa)); cos_of_angle];
         field = in_place * field;
         update(4) = update(4) - fn_gausslegendre(@n_integrand, gl_prm);

         field = [- (pcontour(1) * sin_of_tilt * sin_of_angle + ...
                     pcontour(2) * cos_of_tilt * cos_of_angle); ...
                     pcontour(1) * cos_of_tilt * sin_of_angle - ...
                     pcontour(2) * sin_of_tilt * cos_of_angle];
         update(5) = update(5) - fn_gausslegendre(@n_integrand, gl_prm) * pi / 180;

      otherwise
         for param_index = (sample_index-1):(sample_index+2)
            if 0 == param_index
               mod_index = size(pcontour,2);
            elseif param_index > size(pcontour,2)
               mod_index = param_index - size(pcontour,2);
            else
               mod_index = param_index;
            end
            field = [weights{3 - param_index + sample_index}; ...
                     zeros(size(abscissa))];
            update(1,mod_index) = update(1,mod_index) - fn_gausslegendre(@n_integrand, gl_prm);

            field = flipud(field);
            update(2,mod_index) = update(2,mod_index) - fn_gausslegendre(@n_integrand, gl_prm);
         end
   end
end


   function integrand = n_integrand(unused)
      integrand = hires_velocities .* dot(field, hires_normals);
   end
end
