%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function amplitude = icip2006_amplitude(samples, segm_context, algo_context, acontour_computation, mask_computation, user_data)

planes              = user_data.planes;
means_of_object     = segm_context.means_of_object;
means_of_background = segm_context.means_of_background;

plane1_at_samples = interp2(planes(:,:,1), samples(2,:), samples(1,:));
plane2_at_samples = interp2(planes(:,:,2), samples(2,:), samples(1,:));
plane3_at_samples = interp2(planes(:,:,3), samples(2,:), samples(1,:));

amplitude = (sum(means_of_background.^2) - sum(means_of_object.^2)) - ...
   (2 * (means_of_background(1) - means_of_object(1))) * plane1_at_samples - ...
   (2 * (means_of_background(2) - means_of_object(2))) * plane2_at_samples - ...
   (2 * (means_of_background(3) - means_of_object(3))) * plane3_at_samples;
amplitude(isnan(amplitude)) = 0;
