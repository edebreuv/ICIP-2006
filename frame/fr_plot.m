%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function o_handle = fr_plot(frame, o_options)
%
%1 component:
%class 'double': frame = 1 + ((scale_length - 1) / (maximum - minimum)) * (frame - minimum)
%class 'uint16': frame = uint16(round((((scale_length - 1) / (maximum - minimum)) * (double(frame) - minimum))))
%class 'uint8':  frame = uint8( round((((scale_length - 1) / (maximum - minimum)) * (double(frame) - minimum))))

minimum = double(min(frame(:)));
maximum = double(max(frame(:)));

color_limits = [];
data_mapping = '';

if size(frame,3) == 3
   switch class(frame)
      case 'double'
         if (minimum ~= 0) || (maximum ~= 1)
            if minimum == maximum
               minimum = 0;
               maximum = 1;
            end
            frame = (1 / (maximum - minimum)) * (frame - minimum);
         end

      case 'uint16'
         if (minimum ~= 0) || (maximum ~= 65535)
            if minimum == maximum
               minimum = 0;
               maximum = 65535;
            end
            frame = uint16(round(((65535 / (maximum - minimum)) * (double(frame) - minimum))));
         end

      case {'uint8', 'logical'}
         if (minimum ~= 0) || (maximum ~= 255)
            if minimum == maximum
               minimum = 0;
               maximum = 255;
            end
            frame = uint8(round(((255  / (maximum - minimum)) * (double(frame) - minimum))));
         end
   end
else
   data_mapping = 'direct';
   scale_length = 256;

   if strcmp(class(frame), 'double')
      required_minimum = 1;
      required_maximum = scale_length;
   else
      required_minimum = 0;
      required_maximum = scale_length - 1;
   end

   if (minimum ~= required_minimum) || (maximum ~= required_maximum)
      if minimum == maximum
         minimum = required_minimum;
         maximum = required_maximum;
      end
      color_limits = [minimum maximum];
      data_mapping = 'scaled';
   end
end

if isempty(data_mapping)
   handle = image(frame);
else
   handle = image(frame, 'CDataMapping', data_mapping);
end
if ~isempty(color_limits)
   set(gca, 'CLim', color_limits)
elseif ~ishold(gca)
   set(gca, 'CLimMode', 'auto')
end
if size(frame,3) ~= 3
   colormap(gray(scale_length))
end

if nargin > 1
   if any(o_options == 'i')
      axis image
      o_options = o_options(o_options ~= 'i');
   end
   if any(o_options == 'o')
      axis off
   end
   if any(o_options == 'c')
      colorbar
   end
   image_size = find(o_options == 't');
   if ~isempty(image_size)
      if image_size(1) < length(o_options)
         factor = str2num(o_options(image_size(1) + 1));
         if isempty(factor)
            factor = 0;
         end
      else
         factor = 0;
      end
      screen_size = get(0, 'ScreenSize');
      screen_size = screen_size([3 4]);
      image_size = size(frame);
      image_size = image_size([1 2]);
      if factor == 0
         factor = min(0.4 * screen_size ./ image_size);
      end
      size_max = min(0.74 * screen_size ./ image_size) * image_size;
      image_size = max(factor * image_size, 100 * image_size / max(image_size));
      image_size = min(size_max, image_size);

      truesize(image_size)
   end
end

if nargout > 1
   o_handle = handle;
end
