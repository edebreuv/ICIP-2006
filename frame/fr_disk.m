%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function disk = fr_disk(framesize, center, radius, o_inside, o_outside, o_which_norm)

%FR_DISK Instantiation of a 2-D or 3-D image with constant background containing a constant disk or ball
%
%   D = fr_disk(S, C, R, o_I, o_O, o_N) returns an image with S(1) pixels
%   vertically and S(2) pixels horizontally, whose background has a value
%   of zero or optionally o_O, containing a disk with center C (C(1):
%   vertical coordinate, C(2): horizontal coordinate), a value of 1 or
%   optionally o_I, and a radius of R in L2-norm or optionally o_N-norm
%   where o_N must be zero for L-infinity, one for L1 or two for L2.
%
%   If S is an integer, a 2-D image of size [S S] is returned. If the
%   length of S is 3, a 3-D image containing a ball is returned.
%
%   If C is empty, it is set to 0.5 + S / 2 to center the disk/ball within
%   the image. If C is a real number, it is replicated to match the image
%   dimension.
%
%   If the length of R is equal to the image dimension, an elongated
%   disk/ball aligned with the axes is produced. Otherwise, R must be a
%   real number.
%
%   Example
%      figure
%      D0 = fr_disk([120 80], [80.5 40.5], [40 35], 1, 0, 0);
%      subplot(2,2,1), title('Norm Infinity'), imagesc(D0), axis image, colormap('gray')
%      D1 = fr_disk([120 80], [80.5 40.5], 35, 1, 0, 1);
%      subplot(2,2,2), title('Norm 1'), imagesc(D1), axis image, colormap('gray')
%      D2 = fr_disk([120 80], [80.5 40.5], [40 35], 1, 0, 2);
%      subplot(2,2,3), title('Norm 2'), imagesc(D2), axis image, colormap('gray')
%      subplot(2,2,4), title('Sum of the 3 norms'), imagesc(D0 + D1 + D2), axis image, colormap('gray')
%
%Eric Debreuve
%Last update: September 30, 2011

if exist('o_inside', 'var')
   if exist('o_outside', 'var')
      if ~exist('o_which_norm', 'var')
         o_which_norm = 2;
      end
   else
      o_outside    = 0;
      o_which_norm = 2;
   end
else
   o_inside     = 1;
   o_outside    = 0;
   o_which_norm = 2;
end

if length(framesize) == 1
   framesize = [framesize framesize];
end

if isempty(center)
   center = 0.5 + framesize / 2;
elseif length(center) == 1
   center = repmat(center, 1, length(framesize));
end

if length(framesize) > 2
   [coords_1, coords_2, coords_3] = ndgrid(1:framesize(1), 1:framesize(2), 1:framesize(3));
else
   [coords_1, coords_2] = ndgrid(1:framesize(1), 1:framesize(2));
   coords_3 = 0;
   center = [center 0];
end

if length(radius) == 1
   switch o_which_norm
      case 0, disk = o_inside * (max(abs(coords_1 - center(1)), max(abs(coords_2 - center(2)), abs(coords_3 - center(3)))) < radius);
      case 1, disk = o_inside * (abs(coords_1 - center(1)) + abs(coords_2 - center(2)) + abs(coords_3 - center(3)) <= radius);
      case 2, disk = o_inside * ((coords_1 - center(1)).^2 + (coords_2 - center(2)).^2 + (coords_3 - center(3)).^2 < radius*radius);
   end
else
   if length(radius) == 2
      radius = [radius 1];
   end
   switch o_which_norm
      case 0, disk = o_inside * (max(abs(coords_1 - center(1)) / radius(1), max(abs(coords_2 - center(2)) / radius(2), abs(coords_3 - center(3)) / radius(3))) < 1);
      case 1, disk = o_inside * (abs(coords_1 - center(1)) / radius(1) + abs(coords_2 - center(2)) / radius(2) + abs(coords_3 - center(3)) / radius(3) <= 1);
      case 2, disk = o_inside * ((coords_1 - center(1)).^2 / radius(1)^2 + (coords_2 - center(2)).^2 / radius(2)^2 + (coords_3 - center(3)).^2 / radius(3)^2 < 1);
   end
end

disk(disk == 0) = o_outside;
