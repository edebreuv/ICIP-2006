%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function resized = fr_resizing(frame, other_size, o_method)
%resized matrix = fr_resizing(
%   frame: ndims(.) == 2 or ndims(.) == 3 (usually size(.,3) == 3)
%   other_size = a string understood by fr_size or [a b], or 2 or 4
%   for h26l (half of a pixel or quarter of a pixel, resp.)
%   o_method {'linear'}: parameter of interp2

if numel(other_size) > 1
   if nargin < 3
      o_method = 'linear';
   end

   other_size = fr_size('size', other_size);
   if isempty(other_size)
      resized = frame;
      return
   end

   rows = 1 + ((size(frame,1) - 1) / (other_size(1) - 1)) * (0:other_size(1) - 1);
   rows(end) = size(frame,1);

   cols = 1 + ((size(frame,2) - 1) / (other_size(2) - 1)) * (0:other_size(2) - 1);
   cols(end) = size(frame,2);

   if ndims(frame) == 2
      resized = interp2(frame, cols, transpose(rows), o_method);
   else
      resized = zeros([other_size size(frame,3)]);
      for dimension = 1:size(frame,3)
         resized(:,:,dimension) = interp2(frame(:,:,dimension), cols, transpose(rows), o_method);
      end
   end
else
   quarter = (other_size == 4);

   if ndims(frame) == 2
      resized = l_h26l(frame);
      if quarter
         resized = fr_resizing(resized, 2 * (size(resized) - 1) + 1, 'linear');
         return
      end
   else
      resized = zeros([1 1 0] + [other_size other_size 1] .* (size(frame) - [1 1 0]));
      for dimension = 1:size(frame,3)
         tmp_resized = l_h26l(frame(:,:,dimension));
         if quarter
            resized(:,:,dimension) = fr_resizing(tmp_resized, 2 * (size(tmp_resized) - 1) + 1, 'linear');
         end
      end
      if quarter
         return
      end
   end
end

resized(resized < 0)   = 0;
resized(resized > 255) = 255;



function resized = l_h26l(frame)

resized = zeros(2 * (size(frame) - 1) + 1);
resized(1:2:end, 1:2:end) = frame;

h26l_filter = (1 / 32) * [1; -5; 20; 20; -5; 1];

interpolation = imfilter(frame, h26l_filter, 'replicate');
resized(2:2:end, 1:2:end) = interpolation(1:(end-1), :);

interpolation = imfilter(resized(:, 1:2:end), transpose(h26l_filter), 'replicate');
resized(:, 2:2:end) = interpolation(:, 1:(end-1));
