%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function framesize = fr_size(task, o_argument)
%
%'size', 'cif'                        -> [288  352]
%'size', [288  352]                   -> [288  352]
%'name', [288  352]                   -> 'cif'
%'name', 'cif'                        -> 'cif'
%'intuition', 'c:/foreman_cif300.yuv' -> 'cif'
%'standards'                          -> {'vga', 'svga' ... 'sqcif', 'qcif' ... 'd4' 'd16'}
%'standards', 'computer'              -> {'vga', 'svga' ...}
%'standards', 'tv'                    -> {'sqcif', 'qcif' ...}
%'standards', 'cinema'                -> {'d4' 'd16'}
%'dictionary': same as 'standards' with sizes returned in a nx2 cell array

dictionary = {...
   'vga'     [ 480  640]; ...
   'svga'    [ 600  800]; ...
   'xga'     [ 768 1024]; ...
   'qvga'    [ 960 1280]; ...
   'sxgap'   [1050 1400]; ...
   'uxga'    [1200 1600]; ...
   'qxga'    [1536 2048]; ...
   'qsxgap'  [2100 2800]; ...
   'quxga'   [2400 3200]; ...
   'sxga'    [1024 1280]; ...
   'wxga'    [ 800 1280]; ...
   'wsxgap'  [1050 1680]; ...
   'wuxga'   [1200 1920]; ...
%
   'sqcif'   [  96  128]; ...
   'qcif'    [ 144  176]; ...
   'cif'     [ 288  352]; ...
   '4cif'    [ 576  704]; ...
   '9cif'    [ 864 1056]; ...
   '16cif'   [1152 1408]; ...
   'd1ntsc'  [ 480  720]; ...
   'd1pal'   [ 576  720]; ...
   'sif'     [ 240  352]; ...
   '4sif'    [ 480  704]; ...
   'sifntsc' [ 240  360]; ...
   'sifpal'  [ 288  360]; ...
   'hdtv60'  [ 720 1280]; ...
   'hdtv30'  [1080 1920]; ...
%
   'd4'      [1024 1440]; ...
   'd16'     [2048 2880]  ...
};

switch task
   case 'size'
      if ischar(o_argument)
         found = strmatch(o_argument, dictionary(:,1), 'exact');
         if isempty(found)
            framesize = [];
         else
            framesize = dictionary{found,2};
         end
      elseif isnumeric(o_argument) && (numel(o_argument) == 2)
         framesize = o_argument;
      else
         framesize = [];
      end

   case 'name'
      if ischar(o_argument)
         framesize = o_argument;
      elseif isnumeric(o_argument) && (numel(o_argument) == 2)
         framesize = '';
         for size_index = 1:size(dictionary,1)
            if isequal(dictionary{size_index,2}, o_argument)
               framesize = dictionary{size_index,1};
               break
            end
         end
      else
         framesize = '';
      end

   case 'intuition'
      if ~isempty(strfind(o_argument, 'svga'))
         framesize = 'svga';
      elseif ...
         ~isempty(strfind(o_argument, 'qvga'))
         framesize = 'qvga';
      elseif ...
         ~isempty(strfind(o_argument, 'vga'))
         framesize = 'vga';
      elseif ...
         ~isempty(strfind(o_argument, 'qsxgap'))
         framesize = 'qsxgap';
      elseif ...
         ~isempty(strfind(o_argument, 'wsxgap'))
         framesize = 'wsxgap';
      elseif ...
         ~isempty(strfind(o_argument, 'sxgap'))
         framesize = 'sxgap';
      elseif ...
         ~isempty(strfind(o_argument, 'sxga'))
         framesize = 'sxga';
      elseif ...
         ~isempty(strfind(o_argument, 'quxga'))
         framesize = 'quxga';
      elseif ...
         ~isempty(strfind(o_argument, 'wuxga'))
         framesize = 'wuxga';
      elseif ...
         ~isempty(strfind(o_argument, 'uxga'))
         framesize = 'uxga';
      elseif ...
         ~isempty(strfind(o_argument, 'qxga'))
         framesize = 'qxga';
      elseif ...
         ~isempty(strfind(o_argument, 'wxga'))
         framesize = 'wxga';
      elseif ...
         ~isempty(strfind(o_argument, 'xga'))
         framesize = 'xga';
%
      elseif ...
         ~isempty(strfind(o_argument, 'sqcif'))
         framesize = 'sqcif';
      elseif ...
         ~isempty(strfind(o_argument, 'qcif'))
         framesize = 'qcif';
      elseif ...
         ~isempty(strfind(o_argument, '16cif'))
         framesize = '16cif';
      elseif ...
         ~isempty(strfind(o_argument, '9cif'))
         framesize = '9cif';
      elseif ...
         ~isempty(strfind(o_argument, '4cif'))
         framesize = '4cif';
      elseif ...
         ~isempty(strfind(o_argument, 'cif'))
         framesize = 'cif';
      elseif ...
         ~isempty(strfind(o_argument, 'd1ntsc'))
         framesize = 'd1ntsc';
      elseif ...
         ~isempty(strfind(o_argument, 'd1pal'))
         framesize = 'd1pal';
      elseif ...
         ~isempty(strfind(o_argument, 'sifntsc'))
         framesize = 'sifntsc';
      elseif ...
         ~isempty(strfind(o_argument, 'sifpal'))
         framesize = 'sifpal';
      elseif ...
         ~isempty(strfind(o_argument, '4sif'))
         framesize = '4sif';
      elseif ...
         ~isempty(strfind(o_argument, 'sif'))
         framesize = 'sif';
      elseif ...
         ~isempty(strfind(o_argument, 'hdtv60'))
         framesize = 'hdtv60';
      elseif ...
         ~isempty(strfind(o_argument, 'hdtv30'))
         framesize = 'hdtv30';
%
      elseif ...
         ~isempty(strfind(o_argument, 'd16'))
         framesize = 'd16';
      elseif ...
         ~isempty(strfind(o_argument, 'd4'))
         framesize = 'd4';
      else
         framesize = '';
      end

   case 'standards'
      framesize = dictionary(:,1);

   case 'dictionnary'
      framesize = dictionary;
end
