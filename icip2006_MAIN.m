%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

title = 'ICIP 2006';

%parameters
   %data
   frame_file                 = 'resource/cell.jpg';
   resizing_factor            = 1/3;
   user_data.resolution       = 40;

   %active contour
   constraint                 = 'circle';%'o-c circle', 'circle', 'c ellipse', 'e ellipse', 'splineX' where X is an integer (e.g., spline15)
   interactive                = false;
   segm_prm.iteration_limit   = 200;
   %'o-c circle' = off-center circle (ignores interactive status)
   %'c ellipse'  = circular   ellipse as an initialization
   %'e ellipse'  = elliptical ellipse as an initialization

   pcontour_prm.step_limit    = -30;%-5 for ellipse from circle

   %interface
   interface_prm.pcontour     = @gi_acontour;
   interface_prm.interruption = @gi_acontour;

%data
   %image
   user_data.planes = double(imread(frame_file));
   user_data.framesize = size(user_data.planes);
   user_data.framesize(end) = [];
   user_data.framesize = round(resizing_factor * user_data.framesize);
   user_data.planes = fr_resizing(user_data.planes, user_data.framesize);

%initial segmentation
   %instanciation
   if interactive
      hofigure = figure('NumberTitle', 'off', 'Name', 'Initial segmentation', ...
      'Color', 'white', 'Menubar', 'none', 'Toolbar', 'none');
      hoaxes = axes('Parent', hofigure);
      fr_plot(user_data.planes)
      geometry = get(hoaxes, 'TightInset');
      set(hoaxes, 'Position', [geometry(1), geometry(2), 1-geometry(3)-geometry(1), 1-geometry(4)-geometry(2)]);
      axis image off
      text('Parent', hoaxes, 'Position', [10 10], 'Color', 'g', ...
         'String', 'Press the Return key when done')
      set(hofigure, 'KeyPressFcn', @c_keypress)
      hocircle = imellipse(hoaxes, [fliplr(user_data.framesize / 4), repmat(mean(user_data.framesize / 2), 1, 2)]);%fliplr(user_data.framesize / 2)
      api = iptgetapi(hocircle);
      api.setFixedAspectRatioMode(true)
      uiwait(hofigure)
      boundingbox = api.getPosition();
      close(hofigure)
      center = boundingbox([2 1]) + (boundingbox([4 3]) / 2);
      radius = boundingbox(3) / 2;
   else
      center = user_data.framesize / 2;
      radius = user_data.framesize(1) / 4;
   end
   switch constraint
      case 'o-c circle'%off-center circle: background segmentation
         pcontour = [user_data.framesize./[1.3 1.8], user_data.framesize(1)/6];

      case 'circle'
         pcontour = [center radius];%gi_shape(user_data.planes, 'forced circle', 'forced parameters');

      case 'c ellipse'%circular ellipse as an initialization
         pcontour = [center radius radius 0];

      case 'e ellipse'%elliptical ellipse as an initialization
         pcontour = [center, radius/2, radius, 0];

      otherwise%spline with n control points (3, 4, 5, 6, 9, 15)
         nctrl_points = str2double(constraint(7:end));
         pcontour = po_circle(center, radius, nctrl_points);
         pcontour(:,end) = [];
         control_samples = diag(2/3 * ones(1,size(pcontour,2)));
         control_samples(1,size(pcontour,2)) = 1/6;
         control_samples(size(pcontour,2),1) = 1/6;
         other_diagonal = diag(ones(1,size(pcontour,2)) / 6, 1);
         other_diagonal = other_diagonal(1:size(pcontour,2),1:size(pcontour,2));
         control_samples = control_samples + other_diagonal + transpose(other_diagonal);
         control_samples = inv(control_samples);
         pcontour(1,:) = transpose(control_samples * transpose(pcontour(1,:)));
         pcontour(2,:) = transpose(control_samples * transpose(pcontour(2,:)));
   end

   %display
   acontour = icip2006_acontour(pcontour, 's', user_data);
   gi_acontour('initialization', title, user_data.planes, acontour)

%segmentation process
   [segm_context, algo_context] = pc_segmentation(...
      user_data, pcontour, @icip2006_validity, @icip2006_acontour, [], ...
      @icip2006_energy, @icip2006_amplitude, @icip2006_update, ...
      segm_prm, pcontour_prm, interface_prm);

%final segmentation
   disp(algo_context)
   disp(segm_context)
   final_acontour = icip2006_acontour(segm_context.pcontour, 's', user_data);
   gi_acontour('result', user_data.planes, acontour, final_acontour)
