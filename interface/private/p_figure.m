%Copyright CNRS/UNS
%Contributor(s): Eric Debreuve (between 2006 and 2010)
%
%eric.debreuve@cnrs.fr
%
%This software is a computer program whose purpose is to segment an
%object in a two-dimensional image, where an object is defined as
%a parametric shape (typically a circle, an ellipse, a spline defined
%by a given number of control points, ...).
%
%This software is governed by the CeCILL  license under French law and
%abiding by the rules of distribution of free software.  You can  use,
%modify and/ or redistribute the software under the terms of the CeCILL
%license as circulated by CEA, CNRS and INRIA at the following URL
%"http://www.cecill.info".
%
%As a counterpart to the access to the source code and  rights to copy,
%modify and redistribute granted by the license, users are provided only
%with a limited warranty  and the software's author,  the holder of the
%economic rights,  and the successive licensors  have only  limited
%liability.
%
%In this respect, the user's attention is drawn to the risks associated
%with loading,  using,  modifying and/or developing or reproducing the
%software by the user in light of its specific status of free software,
%that may mean  that it is complicated to manipulate,  and  that  also
%therefore means  that it is reserved for developers  and  experienced
%professionals having in-depth computer knowledge. Users are therefore
%encouraged to load and test the software's suitability as regards their
%requirements in conditions enabling the security of their systems and/or
%data to be ensured and,  more generally, to use and operate it in the
%same conditions as regards security.
%
%The fact that you are presently reading this means that you have had
%knowledge of the CeCILL license and that you accept its terms.

function [figure_handle, varargout] = p_figure(framesize, naxes, varargin)

figure_handle = figure('Units', 'pixels', varargin{:});

varargout = cell(1, prod(naxes));
for axes_idx = 1:prod(naxes)
   varargout{axes_idx} = axes('Parent', figure_handle, ...
      'XLim', [1 framesize(2)], 'YLim', [1 framesize(1)], ...
      'YDir', 'reverse', 'TickDir', 'out', 'Units', 'pixels');
   % no DataAspectRatio here otherwise margins(2) is equal to zero (is it normal?)
end
margins = get(varargout{1}, 'TightInset');

axes_separation_1 = 2;
axes_separation_2 = 2;

width  = framesize(2) + margins(1) + margins(3) + axes_separation_2;
height = framesize(1) + margins(2) + margins(4) + axes_separation_1;

figure_position = get(figure_handle, 'Position');
figure_size = [naxes(2) * width - axes_separation_2, naxes(1) * height - axes_separation_1];
set(figure_handle, 'Position', [figure_position(1) - round(0.5 * (figure_size(1) - figure_position(3))), ...
   figure_position(2) - figure_size(2) + figure_position(4), figure_size])
% figure_position(2) ensures that the figure bar stays within the screen limits

axes_idx = 1;
for col_idx = 0:(naxes(2)-1)
   for row_idx = (naxes(1)-1):-1:0
      set(varargout{axes_idx}, 'Position', [margins(1) + col_idx * width, ...
         margins(2) + row_idx * height, framesize(2), framesize(1)], ...
         'DataAspectRatio', [1 1 1], 'Units', 'normalized')
      % why DataAspectRatio? for p_hardcopy. only for that?
      % set the axes unit to normalized for autoresizing when the figure is resized
      axes_idx = axes_idx + 1;
   end
end
